/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hipster.tem.web.rest.vm;
